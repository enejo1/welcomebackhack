/* 
 *  Programmed by Tucker Guen and Jeremiah Ukwela
*/

//Sensor Pins
const int trigPinL = 13;
const int echoPinL = 12;
const int trigPinR = 11;
const int echoPinR = 10;

//LED Pins
const int detectionPinL = 7;
const int detectionPinR = 5;
const int powerOnPin = 6;

//Sound Pin
const int soundPin = 8;
const int frequency = 800;

//Calculation Variables
long durationL;
long durationR;
int distanceL;
int distanceR;
int maxDistance = 400;

//Hazardous Distance Variable
int distanceDist = 15;

//Buzzer Active?
bool sound = false;

void setup() {
  //Setup left side pins
  pinMode(trigPinL, OUTPUT);
  pinMode(echoPinL, INPUT);
  pinMode(detectionPinL, OUTPUT);

  //Setup right side pins
  pinMode(trigPinR, OUTPUT);
  pinMode(echoPinR, INPUT);
  pinMode(detectionPinR, OUTPUT);

  //Power pin
  pinMode(powerOnPin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  digitalWrite(powerOnPin, HIGH);

  //Left side activity
  digitalWrite(trigPinL, LOW);
  delayMicroseconds(2);
  
  digitalWrite(trigPinL, HIGH);
  delayMicroseconds(10);
  
  durationL = pulseIn(echoPinL, HIGH);
  distanceL = durationL*0.034/2;
  
  if(distanceL < maxDistance && distanceL > 0){
    digitalWrite(detectionPinL, HIGH);
    sound=true;
  }
  else
  {
    digitalWrite(detectionPinL, LOW);
    sound=false;
  }
  
  Serial.print("distanceL: ");
  Serial.println(distanceL);

  //Right side activity
  digitalWrite(trigPinR, LOW);
  delayMicroseconds(2);
  
  digitalWrite(trigPinR, HIGH);
  delayMicroseconds(10);
  
  durationR = pulseIn(echoPinR, HIGH);
  distanceR = durationR*0.034/2;
  
  if(distanceR < maxDistance && distanceR > 0){
    digitalWrite(detectionPinR, HIGH);
    sound=true;
  }
  else
  {
    digitalWrite(detectionPinR, LOW);
    sound=false;
  }
  
  Serial.print("distanceR: ");
  Serial.println(distanceR);

  //Hazardous activity
  if(distanceL < distanceDist || distanceR < distanceDist)
  {
    tone(soundPin, frequency);
  }
  else
  {
    noTone(soundPin); 
  }
}
